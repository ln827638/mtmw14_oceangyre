"""
Main Code to run to plot all results

Student Id: ln827638
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.interpolate import RegularGridInterpolator
from scipy import integrate
from TaskC import *
from TaskD_testwith3d import *

# Define the parameters 
fo = 10.**-4     # s^-1
beta = 10.**-11      # m^-1 s^-1
g = 10.      # m s^-2
gamma = 10.**-6      # s^-1
rho = 1000.      # kg m^-3
H = 1000.        # m
tau_o = 0.2     # N m^-2
L = 1e+6       # m
epsilon = gamma/(L*beta)

tmin = 0
tmax = 3*24*60*60    # seconds (1 day)

#Setting up the domain
xmin = 0.
xmax = L
ymin = 0.
ymax = L
NX = 100
NY = 100
dx = (xmax - xmin)/NX
#dx = dx/2                    # Remove # when model is run for resolution 0.5d
dy = (ymax - ymin)/NY
#dy = dy/2                    # Remove # when model is run for resolution 0.5d

# Directional grid coordinate values (Arakawa Grid C)
# Location of u points in the x-direction
x_u = np.arange(NX+1) * dx

# Location of v points in the x-direction
x_v = np.arange(NX) * dx + dx/2.

# Location of eta points in the x-direction
x_eta = np.arange(NX) * dx + dx/2.


# Location of u points in the y-direction
y_u = np.arange(NY) * dy + dy/2.

# Location of v points in the y-direction
y_v = np.arange(NY+1) * dy

# Location of eta points in the y-direction
y_eta = np.arange(NY) * dy + dy/2.



# Implement 2D CFL criteria
dt = 0.707*dx/100
nt = int(tmax/dt)           # CFL criteria satisfied


x = np.zeros(NX)
y = np.zeros(NY)

for k in range(NX):
    x[k] = xmin + k*dx
    y[k] = ymin + k*dy



# Define the wind stress vector, tau
tau_x = -np.cos(np.pi*y_u[:,None]/L)
tau_y = 0
tau = np.zeros((NX,NY))
tau = tau_o*np.array((tau_x, tau_y),dtype=object)
