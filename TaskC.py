"""
Analytical Solution
ln827638
"""

import numpy as np
import matplotlib.pyplot as plt

fo = 10.**-4     # s^-1
beta = 10.**-11      # m^-1 s^-1
g = 10.      # m s^-2
gamma = 10.**-6      # s^-1
rho = 1000.      # kg m^-3
H = 1000.        # m
tau_o = 0.2     # N m^-2
L = 1e+6       # m
epsilon = gamma/(L*beta)

#Setting up the domain
xmin = 0.
xmax = L
ymin = 0.
ymax = L
NX = 100
NY = 100
dx = (xmax - xmin)/NX
dy = (ymax - ymin)/NY
dA = dx*dy                  # Area of one grid cell

# Directional grid coordinate values (Arakawa Grid C)
# Location of u points in the x-direction
x_u = np.arange(NX+1) * dx

# Location of v points in the x-direction
x_v = np.arange(NX) * dx + dx/2.

# Location of eta points in the x-direction
x_eta = np.arange(NX) * dx + dx/2.


# Location of u points in the y-direction
y_u = np.arange(NY) * dy + dy/2.

# Location of v points in the y-direction
y_v = np.arange(NY+1) * dy

# Location of eta points in the y-direction
y_eta = np.arange(NY) * dy + dy/2.

x = np.zeros(NX)
y = np.zeros(NY)

# Anlaytical solution provided by Mushgrave

a = (-1 - np.sqrt(1 + (2*np.pi*epsilon)**2))/(2*epsilon)
b = (-1 + np.sqrt(1 + (2*np.pi*epsilon)**2))/(2*epsilon)

for k in range(NX):
    x[k] = xmin + k*dx
    y[k] = ymin + k*dy

def f1(a, b, x):
    f1 = np.zeros(NX)
    f1 = np.pi*(1 + ((np.exp(a)-1)*np.exp(b*x) + \
          (1 - np.exp(b))*np.exp(a*x))/(np.exp(b)-np.exp(a)))
        
    return f1
print('f1', f1)



def f2(a, b, x):
    f2 = np.zeros(NX)
    f2 = ((np.exp(a) - 1)*b*np.exp(b*x) + \
          (1 - np.exp(b))*a*np.exp(a*x))/(np.exp(b)-np.exp(a))
        
    return f2
print('f2', f2)

f1_L = f1(a, b, x_u/L)
f1_L_eta = f1(a, b, x_eta/L)

f2_L = f2(a, b, x/L)
f2_L_eta = f2(a, b, x_eta/L)

def analytic_sol(x, y, f1_L, f2_L):
    """ Analytical Solutions are calculaed for SWEs"""
    
    # When we define the grid like this, we would get the analytical solution 
    # for Arakawa Grid A
    u_st = np.zeros((NY,NX))
    v_st = np.zeros((NY,NX))
    eta_st = np.zeros((NY,NX))
    
    '''
    # For Arakawa Grid C
    u_st = np.zeros((NY,NX+1))
    v_st = np.zeros((NY+1,NX))
    eta_st = np.zeros((NY,NX))
    '''
    eta_o = 0
    for i in range(NX):
            for j in range(NY):
            
                u_st[i,j] = - tau_o*f1_L_eta[i]*np.cos(np.pi*y_u[j]/L)/(np.pi*gamma*rho*H)
                v_st[i,j] = tau_o*f2_L_eta[i]*np.sin(np.pi*y_v[j]/L)/(np.pi*gamma*rho*H)
                eta_st[i,j] = eta_o + \
                        tau_o*fo*L*((gamma*f2_L_eta[i]*np.cos(np.pi*y_eta[j]/L))/np.pi*fo
                            + (f1_L_eta[i]*(np.sin(np.pi*y_eta[j]/L))*(1 + beta*y_eta[j]/fo)) + \
                            (beta*L*np.cos(np.pi*y_eta[j]/L)/fo*np.pi)/np.pi)

    return u_st, v_st, eta_st

u_st, v_st, eta_st = analytic_sol(x, y, f1_L, f2_L)

print(u_st)
print(v_st)
#print(u_st[:,10])


fig=plt.figure(figsize=(10, 8))
plt.contourf(x, y, u_st)    # if nx and ny already 
plt.colorbar()
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.title('u steady state')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/u_st.png")
plt.show()
plt.clf()


fig=plt.figure(figsize=(10, 8))
plt.contourf(x, y, v_st)
plt.colorbar()
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.title('v steady state')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/v_st.png")
plt.show()
plt.clf()


fig=plt.figure(figsize=(10, 8))
plt.contourf(x, y, eta_st)
plt.colorbar()
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.title('$\eta$ steady state')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/eta_st.png")
plt.show()
plt.clf()
