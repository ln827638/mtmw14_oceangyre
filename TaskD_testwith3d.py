"""
Project 2: A simpliﬁed model of ocean gyres and the Gulf Stream 

Tasks D-E
@author: ln827638
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.interpolate import RegularGridInterpolator
from scipy import integrate
from TaskC import *


# Define the parameters 
fo = 10.**-4     # s^-1
beta = 10.**-11      # m^-1 s^-1
g = 10.      # m s^-2
gamma = 10.**-6      # s^-1
rho = 1000.      # kg m^-3
H = 1000.        # m
tau_o = 0.2     # N m^-2
L = 1e+6       # m
epsilon = gamma/(L*beta)

tmin = 0
tmax = 3*24*60*60    # seconds (1 day)

#Setting up the domain
xmin = 0.
xmax = L
ymin = 0.
ymax = L
NX = 100
NY = 100
dx = (xmax - xmin)/NX
#dx = dx/2                    # Remove # when model is run for resolution 0.5d
dy = (ymax - ymin)/NY
#dy = dy/2                    # Remove # when model is run for resolution 0.5d

# Directional grid coordinate values (Arakawa Grid C)
# Location of u points in the x-direction
x_u = np.arange(NX+1) * dx

# Location of v points in the x-direction
x_v = np.arange(NX) * dx + dx/2.

# Location of eta points in the x-direction
x_eta = np.arange(NX) * dx + dx/2.


# Location of u points in the y-direction
y_u = np.arange(NY) * dy + dy/2.

# Location of v points in the y-direction
y_v = np.arange(NY+1) * dy

# Location of eta points in the y-direction
y_eta = np.arange(NY) * dy + dy/2.



# Implement 2D CFL criteria
dt = 0.707*dx/100
nt = int(tmax/dt)           # CFL criteria satisfied


x = np.zeros(NX)
y = np.zeros(NY)

for k in range(NX):
    x[k] = xmin + k*dx
    y[k] = ymin + k*dy



# Define the wind stress vector, tau
tau_x = -np.cos(np.pi*y_u[:,None]/L)
tau_y = 0
tau = np.zeros((NX,NY))
tau = tau_o*np.array((tau_x, tau_y),dtype=object)


# Make 3d arrays with time as the third dimension
# nx and ny were flipped for accounting for latitude and longitude and how
# python tends to read rows and columns
u = np.zeros((NY,NX+1,nt))
v = np.zeros((NY+1,NX,nt))
eta = np.zeros((NY,NX,nt))
E = np.zeros(nt)
time = np.zeros(nt)


def numerical_sol(u, v, eta, E, time):

    for it in range(0,nt-2,2):
        eta[:,:,it+1] = eta[:,:,it] - H*dt*(((u[:,1:,it]- u[:,0:-1,it])/dx) + \
                    ((v[1:,:,it] - v[0:-1,:,it])/dy))
        # In this way we have already applied the boundary conditions
        
        # Perform interpolation of v velocity field onto u points (v_on_u)
        #   interp2d format: (x-column coordinate values, y-row coordinate values, 2d array[y-row, x-column]
        ifunction = interpolate.interp2d(x_v, y_v, v[:,:,it], kind='linear')

        #   run the interpolation function (output x-column coordinate values, output y-row coordinate values)
        v_on_u = ifunction(x_u, y_u)        # v_on_u.shape should equal u[:,:,it].shape
        
        #print('shapes', v_on_u.shape, u.shape)

        u[:,1:-1,it+1] = u[:,1:-1,it] + (fo + beta*y_u[:,None])*dt*v_on_u[:,1:-1] - \
                    g*dt*((eta[:,1:,it+1] - eta[:,0:-1,it+1])/dx) - gamma*dt*u[:,1:-1,it] + \
                        tau_x*dt/(rho*H)
        
        #print(u.shape, y.shape, v.shape)
        
        
        # Perform interpolation of u velocity field onto v points (u_on_v)
        #   interp2d format: (x-column coordinate values, y-row coordinate values, 2d array[y-row, x-column]
        ifunction = interpolate.interp2d(x_u, y_u, u[:,:,it+1], kind='linear')

        #   run the interpolation function (output x-column coordinate values, output y-row coordinate values)
        u_on_v = ifunction(x_v, y_v)        # u_on_v.shape should equal v[:,:,it].shape
        
        
        v[1:-1,:,it+1] = v[1:-1,:,it] - (fo + beta*y_v[1:-1,None])*dt*u_on_v[1:-1,:] -\
                    g*dt*((eta[1:,:,it+1] - eta[0:-1,:,it+1])/dy) - gamma*dt*v[1:-1,:,it] + \
                        tau_y*dt/(rho*H)
                        
        
        time[it+1] = (it+1)*dt
        
        
        
        # Solving v before u
        eta[:,:,it+2] = eta[:,:,it+1] - H*dt*(((u[:,1:,it+1]- u[:,0:-1,it+1])/dx) + \
                    ((v[1:,:,it+1] - v[0:-1,:,it+1])/dy))
        
        # Perform interpolation of u velocity field onto v points (u_on_v)
        #   interp2d format: (x-column coordinate values, y-row coordinate values, 2d array[y-row, x-column]
        ifunction = interpolate.interp2d(x_u, y_u, u[:,:,it+1], kind='linear')

        #   run the interpolation function (output x-column coordinate values, output y-row coordinate values)
        u_on_v = ifunction(x_v, y_v)        # u_on_v.shape should equal v[:,:,it].shape
        
        
        v[1:-1,:,it+2] = v[1:-1,:,it+1] - (fo + beta*y_v[1:-1,None])*dt*u_on_v[1:-1,:] -\
                    g*dt*((eta[1:,:,it+2] - eta[0:-1,:,it+2])/dy) - gamma*dt*v[1:-1,:,it+1] + \
                        tau_y*dt/(rho*H)
        
        # Perform interpolation of v velocity field onto u points (v_on_u)
        #   interp2d format: (x-column coordinate values, y-row coordinate values, 2d array[y-row, x-column]
        ifunction = interpolate.interp2d(x_v, y_v, v[:,:,it+2], kind='linear')

        #   run the interpolation function (output x-column coordinate values, output y-row coordinate values)
        v_on_u = ifunction(x_u, y_u)        # v_on_u.shape should equal u[:,:,it].shape
        
            
        u[:,1:-1,it+2] = u[:,1:-1,it+1] + (fo + beta*y_u[:,None])*dt*v_on_u[:,1:-1] - \
                    g*dt*((eta[:,1:,it+2] - eta[:,0:-1,it+2])/dx) - gamma*dt*u[:,1:-1,it+1] + \
                        tau_x*dt/(rho*H)
        
        # Energy over time
        ifunction = interpolate.interp2d(x_u, y_u, u[:,:,it], kind='linear')
        u_on_eta = ifunction(x_eta, y_eta)
        ifunction = interpolate.interp2d(x_v, y_v, v[:,:,it], kind='linear')
        v_on_eta = ifunction(x_eta, y_eta) 
        E[it] = np.sum((0.5*rho*(H*((u_on_eta**2) + v_on_eta**2) + g*eta[:,:,it]**2))*dx*dy)
        
        
        ## This is probably not giving the correct values as it is adding on to the points
        ## which are repeating in the loop
        # Energy over time
        ifunction = interpolate.interp2d(x_u, y_u, u[:,:,it+1], kind='linear')
        u_on_eta = ifunction(x_eta, y_eta)
        ifunction = interpolate.interp2d(x_v, y_v, v[:,:,it+1], kind='linear')
        v_on_eta = ifunction(x_eta, y_eta) 
        E[it+1] = np.sum((0.5*rho*(H*((u_on_eta**2) + v_on_eta**2) + g*eta[:,:,it+1]**2))*dx*dy)
        
        time[it+2] = (it+2)*dt
        
    return u, v, eta, E, u_on_eta, v_on_eta



u, v, eta, E, u_on_eta, v_on_eta = numerical_sol(u, v, eta, E, time)
eta_no_mod = eta[0,50,-5]
print(E)

fig=plt.figure(figsize=(10, 8))
plt.plot(time[:-2], E[:-2])
plt.xlim(0,86400)
plt.xlabel('Time (s)')
plt.ylabel('Energy')
plt.title('Energy Evolution')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/E_d.png")
plt.show()
plt.clf()
plt.show()

fig=plt.figure(figsize=(10, 8))
plt.plot(x_u, u[0,:,-2])
plt.xlabel('x (m)')
plt.ylabel('u (m/s)')
plt.title('')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/u_vs_x_d.png")
plt.show()
plt.clf()
plt.show()

fig=plt.figure(figsize=(10, 8))
plt.plot(v[:,0,-2], y_v)
plt.ylabel('y (m)')
plt.xlabel('v (m/s)')
plt.title('')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/v_vs_y_d.png")
plt.show()
plt.clf()


fig=plt.figure(figsize=(10, 8))
plt.plot(x_eta, eta[50,:,-2])
plt.xlabel('x (m)')
plt.ylabel('$\eta$')
plt.title('')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/eta_vs_x_d.png")
plt.show()
plt.clf()
plt.show()

fig=plt.figure(figsize=(10, 8))
plt.contourf(x, y, eta[:,:,-2])
plt.colorbar()
plt.xlabel('x [km]')
plt.ylabel('y [km]')
plt.title('$\eta$')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/eta_2D_d.png")
plt.legend
plt.show()
plt.clf()

def differences(u_on_eta, u_st, v_on_eta, v_st, eta, eta_st):
    """Calculates the differences/errors (Modelled - Analytical)"""
    u_err = u_on_eta[:,:]- u_st[:,:]
    v_err = v_on_eta[:,:] - v_st[:,:]
    eta_err = eta[:,:,-2] - eta_st[:,:]
    
    return u_err, v_err, eta_err

u_err, v_err, eta_err = differences(u_on_eta, u_st, v_on_eta, v_st, eta, eta_st)
# Energy Error
E_error = np.sum((0.5*rho*(H*((u_err**2) + v_err**2) + g*eta_err**2))*dx*dy)
print('Energy error = ', E_error)


# Error plot (simple)
fig=plt.figure(figsize=(10, 8))
plt.contourf(x/1000,y/1000, u_err)
plt.colorbar()
plt.title("$u - u$")
plt.xlabel('x [km]')
plt.ylabel('y [km]')
plt.gca().set_aspect('equal', adjustable='box')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/u_err_d.png")
plt.show()
plt.clf()

fig=plt.figure(figsize=(10, 8))
plt.contourf(x/1000,y/1000, v_err)
plt.colorbar()
plt.title("$v-v$")
plt.xlabel('x [km]')
plt.ylabel('y [km]')
plt.gca().set_aspect('equal', adjustable='box')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/v_err_d.png")
plt.show()
plt.clf()

fig=plt.figure(figsize=(10, 8))
plt.contourf(x/1000,y/1000, eta_err)
plt.colorbar()
plt.title("$eta-eta$")
plt.xlabel('x [km]')
plt.ylabel('y [km]')
plt.gca().set_aspect('equal', adjustable='box')
plt.savefig("C:/Users/DELL/Desktop/MTMW14_Plots/eta_err_d.png")
plt.show()
plt.clf()







